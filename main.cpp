#include <iostream>
#include <vector>
#include "pizza.h"
#include <limits>
using namespace std;
vector<cKlasa> vPizza;  //przechowuje zamowienie
string menu[SIZE];    //przechowuje nazwy produktow
float price[SIZE]; //przechowuje ceny produktow
int main()
{
    file_txt(menu, price);
    int free_cola=0;
    bool run=true;
    int choice;
    cout<<"                            Witamy w Pizzerii!!!"<<endl;
    while(run)
    {
        if(free_cola==2)
        {
            add_free_cola(&vPizza);
        }
        if(vPizza.size()!=0)
        {
            display_order(&vPizza);     //wyświetlenie aktualnie składanego zamówienia
        }
        switch_display();
        while(!(cin>>choice)|| (choice < 0 || choice > 4))  //
        {
            cout<<"Blad! Podaj liczbe z przedzialu od 1-4: ";   //  zabezpiecznie przed wpisaniem nieprawidlowych danych
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(),'\n');  //
        }
        switch(choice)
        {
        case 1:
            system("cls");
            display_products(menu,price,0,2);
            int x;
            cout<<"Wybor: ";
            cin>>x;
            if(x==1||x==2)
            {
                add_product(&vPizza, menu, price, x-1);
            }
            else
            {
                cout<<"Blad!"<<endl;
            }
            system("cls");
            break;
        case 2:
            system("cls");
            display_products(menu,price,2,15);
            int y;
            cout<<"Wybor: ";
            cin>>y;
            if(y>0&&y<8)
            {
                free_cola++;
            }
            if(y>0&&y<14)
            {
                add_product(&vPizza, menu, price, y+1);
            }
            else
            {
                cout<<"Blad!"<<endl;
            }
            system("cls");
            break;
        case 3:
            system("cls");
            display_products_promo(menu,price,2,9);
            int z;
            cout<<"Wybor: ";
            cin>>z;
            if(z>0&&z<8)
            {
                add_product_promo(&vPizza, menu, price, z+1);
            }
            else
            {
                cout<<"Blad!"<<endl;
            }
            system("cls");
            break;
        case 4:
            system("cls");
            cout<<"Paragon: "<<endl;
            generate_receipt(&vPizza);
            cout<<"Zapraszamy ponownie!"<<endl;
            run=false;

        }

    }


    return 0;
}
