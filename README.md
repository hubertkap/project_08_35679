# Dokumentacja
```
Projekt tworzony w środowisku Qt Creator(wersja 7.0.1)
Plik "menu.txt" musi znajdować się w folderze build projektu utworzonego podczas pierwszego uruchomienia.
```
## Podział pracy w projekcie:
```
Filip Kiełbasa          Dodanie menu pizzeri
                        Dodanie plików konfiguracyjnych
                        Dodanie klasy w headerze
                        Dodanie funkcji wczytywania z pliku, stworzenie tablic i wektora
                        Dodanie do klasy funkcji wyswietlania zamowienia z ceną produktów
                        Dodanie funkcji napój gratis przy zakupie dwóch pizz
                        Dodanie zabezpieczne przed wpisaniem niepoprawnych danych przez użytkownika

Hubert Kapuściński	Dodanie  instrukcji switch w main
                        Dodanie menu pizzeri
                        Dodanie funkcji wyswietlania aktualnie zamawianego zamowienia
                        Dodanie funkcji generowania paragonu
                        Utworzenie projektu		

Kamil Pacuła		Dodanie funkcji wyswietlania zestawów
                        Dodanie funkcji dodawania produktów do wektora
                        Dodanie funkcji dodawania pizzy 50/50 do zamówienia
                        Stworzenie  pliku Readme.md
```
    
## Autorzy
```
Hubert Kapuściński
Filip Kiełbasa
Kamil Pacuła
```
