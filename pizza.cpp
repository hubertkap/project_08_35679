#include "pizza.h"
#include <fstream>
#include <vector>

using namespace std;

void file_txt(string menu[], float price[])
{
    string temp;
    int l = 1;  //linie pliku wczytywane getline
    int m = 0;  //indeks produktow w menu
    int p = 0; //indeks cen produktow

    fstream file;
    file.open("menu.txt", ios::in); //plik tylko do odczytu

    if(file.good()==false)
    {
        cout << "Plik nieistnieje!";
        exit(1);
    }

    while(getline(file, temp))
    {
        if(l > 0 && l <= 17)
        {
            menu[m] = temp;
            m++;
        }

        else if(l > 17)
        {
            price[p] = stof(temp);       //zamiana na float
            p++;
        }
        l++;
    }
    file.close();
}
void display_products(string menu[],float price[], int begin, int end)
{
    int count = 1;

    for(int i = begin; i < end; i++)
    {

        cout << count << ". " << menu[i] << "- " << price[i] << "zl" << endl;
        count++;
    }

}
void display_products_promo(string menu[],float price[], int begin, int end)
{
    int count = 1;

    for(int i = begin; i < end; i++)
    {
        cout << count<< ". " << menu[i] << "(50/50) -" << price[i]*0.5 << "zl" << endl;
        count++;
    }

}
void add_product(vector<cKlasa> *vPizza, string menu[], float price[], int t)
{

    cKlasa obj;
    int size = (*vPizza).size(); //rozmiar wektora
    (*vPizza).push_back(obj);
    (*vPizza)[size].set(menu[t]+"-", price[t]);

}
void add_product_promo(vector<cKlasa> *vPizza, string menu[], float price[], int t)
{
    cKlasa obj;
    int size = (*vPizza).size();
    (*vPizza).push_back(obj);
    (*vPizza)[size].set(menu[t]+"50/50", price[t]*0.5);
}
void switch_display()
{
    cout<<"Promocje: "<<endl;
    cout<<"Przy zakupie dwoch pizz coca cola gratis!(nie dotyczy zestawow oraz pizz 50/50)"<<endl;
    cout<<"Przy zakupie powyzej 100zl rabat 20%!"<<endl;
    cout<<"1. Zamow zestaw"<<endl;
    cout<<"2. Zamow pojedynczy produkt"<<endl;
    cout<<"3. Zamow pizze 50/50"<<endl;
    cout<<"4. Zakoncz"<<endl;
    cout<<"Wybor: ";
}
void display_order(vector<cKlasa> *vPizza)
{

    int size = (*vPizza).size();
    float price = 0;
    if(size > 0)
    {
        cout << endl << "Twoje zamowienie: " << endl << endl;
        for(int i = 0; i < size; i++)
        {
            price+=(*vPizza)[i].get();
            cout << i + 1 << ". ";
            (*vPizza)[i].show_order();
        }
        if(price>100)
        {
            price=price*0.8;
            cout<<"Promocja 20% znizki aktywowana";
        }
        cout << endl << "Do zaplaty: " << price << "zl";
    }
    cout << endl << endl;

}
void add_free_cola(vector<cKlasa> *vPizza)
{
        cKlasa obj;
        int size = (*vPizza).size();
        (*vPizza).push_back(obj);
        (*vPizza)[size].set("Coca-Cola 0.5l - ", 0);
}
void generate_receipt(vector<cKlasa> *vPizza)
{
    int size = (*vPizza).size();
    float price=0;
    for(int i=0;i<size;i++)
    {
        (*vPizza)[i].show_order();
        price+=(*vPizza)[i].get();

    }
    if(price>100)
    {
        price=price*0.8;
    }
    if(price>0)
    {
    cout<<"Do zaplaty: "<<price<<"zl"<<endl;
    }
}
