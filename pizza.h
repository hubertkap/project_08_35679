#ifndef PIZZA_H
#define PIZZA_H
#include <iostream>
#include <fstream>
#include <vector>
#define SIZE 17
using namespace std;
class cKlasa
{
private:
    string name;
    float price;
public:
    void show_order()
    {
        cout << name << " " << price << "zl" << endl;
    }
    void set(string name, float price)      //ustawianie prywatnych zmiennych
    {
       this->name = name;
       this->price = price;
    }
    float get()     //zwracanie prywatnego pola
    {
        return price;
    }
};
//prototypy funkcji
void file_txt(string[], float[]);
void display_products(string[],float[],int,int);
void display_products_promo(string[],float[],int,int );
void add_product(vector<cKlasa> *vPizza, string[], float[], int);
void add_product_promo(vector<cKlasa> *vPizza, string[], float[], int );
void switch_display();
void display_order(vector<cKlasa> *vPizza);
void add_free_cola(vector<cKlasa> *vPizza);
void generate_receipt(vector<cKlasa> *vPizza);
#endif // PIZZA_H
