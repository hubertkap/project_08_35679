TEMPLATE = app
CONFIG += console c++17
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.cpp \
        pizza.cpp

HEADERS += \
    pizza.h

DISTFILES += \
    .gitignore \
    README.md
